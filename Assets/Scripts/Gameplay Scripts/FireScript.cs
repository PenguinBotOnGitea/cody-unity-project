using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireScript : MonoBehaviour
{

    //Variables go here
    public Vector2 LForce;
    public Vector2 RForce;
    public float DisappearTime;
    public Rigidbody2D LRB;
    public string EnemiesTag;
    public PlayerMovementScript PMS;
    public string PTag;
    public string GTag;
    public GameObject SelfL;
    public WaveScript WS;
    public bool ReduceCheck;
    // Start is called before the first frame update
    void Start()
    {
     //Debug.Log("I've spawned!");
     PMS = GameObject.FindWithTag(PTag).GetComponent<PlayerMovementScript>();
     WS = GameObject.FindWithTag("BManager").GetComponent<WaveScript>();
     ReduceCheck = true;
     if (PMS.WhichDirection == 2)
     {
      LRB.AddForce(LForce);
     }
     else if (PMS.WhichDirection == 1)
     {
      LRB.AddForce(RForce);
     }
     else if (PMS.WhichDirection == 0)
     {
      if (PMS.LastActiveState == 2)
      {
       LRB.AddForce(LForce);
      }
      else if (PMS.LastActiveState == 1)
      {
       LRB.AddForce(RForce);
      }
     }
     StartCoroutine(DisappearDelay());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator DisappearDelay()
    {
     yield return new WaitForSeconds(DisappearTime);
     Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D Touch)
    {
     if (Touch.gameObject.tag == EnemiesTag)
     {
      //Touch.gameObject.GetComponent<EnemyScript>().DDead();
      //Touch.gameObject.GetComponent<DumbEScript>().DDead();
      //Destroy(Touch);
      //if (ReduceCheck == true) {
      // WS.ReduceEnemy();
      // ReduceCheck = false;
      //Debug.Log("Empty");
      //}
      Destroy(SelfL);
      //Debug.Log("Why won't you destroy yourself?");
     }
     else if (Touch.gameObject.tag == GTag)
     {
      Destroy(gameObject);
     }
    }
}
