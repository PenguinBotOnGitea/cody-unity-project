﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePanelScript : MonoBehaviour
{
    public PauseEndScript PES;
    public bool ShouldShow;
    // Start is called before the first frame update
    void Start()
    {
     gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
     
    }

    public void ShowPanel()
    {
     gameObject.SetActive(true);
    }

    public void HidePanel()
    {
     gameObject.SetActive(false);
    }
}
