using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFireNew : MonoBehaviour
{
    public GameObject BulletPrefab;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void InstantiateBullet() {
     Instantiate(BulletPrefab, new Vector2(5.05f, 0.7f), Quaternion.Euler(0,0,0));
    }
}
