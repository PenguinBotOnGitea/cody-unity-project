using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreScript : MonoBehaviour
{
    //Variables goes here
    public GameObject SText;
    public float Score;
    //public float AddValueDumb;
    //public float AddValueSmart;
    
    // Start is called before the first frame update
    void Start()
    {
     Score = 0;   
    }

    // Update is called once per frame
    void Update()
    {
     SText.GetComponent<UnityEngine.UI.Text>().text = Score.ToString();   
    }

    public void AddScorePlayer(float ScoreValue) {
     Debug.Log("Adding score..");
     Score = Score + ScoreValue;
    }
}
