using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartFire : MonoBehaviour
{
    public Vector2 Force;
    public GameObject Bullets;
    public EnemyDetectorScript EDS;
    public bool Check;
    public int BulletsTotal;
    // Start is called before the first frame update
    void Start()
    {
     BulletsTotal = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Fire() {
     if (Check == true && BulletsTotal < 1) {
      Instantiate(Bullets, new Vector2(5.05f, 0.7f), Quaternion.Euler(0,0,0));
      //Check = false;
      BulletsTotal++;
      }
    }

    public void CheckBool(bool value) {
     Check = value;
    }
}
