using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectorScript : MonoBehaviour
{
    public string ETag;
    public StartFire SF;
    public bool Check;
    public int Gotcha;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D Touch) {
     if (Touch.gameObject.tag == ETag) {
       Debug.Log(Check);
      if (Check == true) {
       SF.CheckBool(true);
       SF.Fire();
       Gotcha++;
       Debug.Log("Gotcha:");
       Debug.Log(Gotcha);
      }
     }
    }

    public void CheckBool(bool value) {
     Check = value;
    }
}
