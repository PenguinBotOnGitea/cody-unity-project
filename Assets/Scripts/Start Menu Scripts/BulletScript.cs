using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public string EnemyTarget;
    public EnemyDetectorScript EDS;
    public StartFire SFS;
    // Start is called before the first frame update
    void Start()
    {
     Debug.Log(transform.position.x);
     GetComponent<Rigidbody2D>().AddForce(new Vector2(-200,0));
     EDS = GameObject.Find("Square").GetComponent<EnemyDetectorScript>();
     SFS = GameObject.Find("Ployor").GetComponent<StartFire>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D Touch) {
     if (Touch.gameObject.tag == EnemyTarget) {
      Destroy(Touch.gameObject);
      EDS.CheckBool(false);
      Debug.Log(EDS.Check);
      SFS.BulletsTotal--;
      Destroy(gameObject);
     }
    }
}
