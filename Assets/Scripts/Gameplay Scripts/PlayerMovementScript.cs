﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour
{
    public Rigidbody2D RB;
    public Vector2 RightForce;
    public Vector2 LeftForce;
    public Vector2 UpForce;
    public string WhichTagGround;
    public bool IsTouchingGround;
    public float WhichDirection;
    public Transform LT;
    public float LastActiveState;
    public PlayerHealthScript PHS;
    // Start is called before the first frame update
    void Start()
    {
     WhichDirection = 0;
     LastActiveState = 1;
    }

    // Update is called once per frame
    void Update()
    {
     if (WhichDirection == 2)
     {
      MoveLeft();
     }
     else if (WhichDirection == 1)
     {
      MoveRight();
     }
     else if (WhichDirection == 0)
     {
      Nothing();
     }
    }

    public void StateRight()
    {
     WhichDirection = 1;
     LastActiveState = 1;
    }

    public void StateLeft()
    {
     WhichDirection = 2;
     LastActiveState = 2;
    }

    public void StateStay()
    {
     WhichDirection = 0;
    }

    public void MoveRight()
    {
     LT.rotation = Quaternion.Euler(0,0,0);
     RB.AddForce(RightForce);
     //Debug.Log("Going to the right side of the screen!");
    }

    public void MoveLeft()
    {
     LT.rotation = Quaternion.Euler(0,0,180);
     RB.AddForce(LeftForce);
     //Debug.Log("Going to the left side of the screen!");
    }

    public void MoveUp()
    {
     if (IsTouchingGround == true)
     {
      RB.AddForce(UpForce);
      //Debug.Log("Going up!");
     }
     else if (IsTouchingGround != true)
     {
      //Debug.Log("No jumping for you");
     }
    }

    void OnTriggerStay2D(Collider2D Touch)
    {
     if (Touch.gameObject.tag == "Ground")
     {
      // Debug.Log("Collided with a game object that has the specific tag, setting variable to true..");
      IsTouchingGround = true;
      // Debug.Log("Successfully changed the variable to true!");
     }
    }

    void OnTriggerExit2D(Collider2D Touch)
    {
     if (Touch == null || Touch.gameObject.tag != "Ground");
     {
      // Debug.Log("Didn't collide to any game object or any game object that has the tag");
      IsTouchingGround = false;
      // Debug.Log("Successfully changed the variable to false!");
     }
    }


    public void Nothing()
    {
     //Debug.Log("*Proceeds to do nothing*");
    }

    public void ReturnToBase() {
     transform.position = new Vector3(0,0,0);
     Debug.Log("RETURN TO BASE DAMN IT");
     PHS.SubstractHealth(1);
    }
}
