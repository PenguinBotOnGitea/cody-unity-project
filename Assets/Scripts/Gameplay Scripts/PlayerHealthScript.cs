﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthScript : MonoBehaviour
{
    public GameObject HValue;
    public float Health;
    public PauseEndScript PES;
    private bool AddCheck;
    // Start is called before the first frame update
    void Start()
    {
     HValue = GameObject.FindWithTag("HValue");
     Health = 100;
     AddCheck = false;
    }

    // Update is called once per frame
    void Update()
    {
     if (Health <= 0)
     {
      PES.IsOver = true;
     }
    }

    void FixedUpdate()
    {
     HValue.GetComponent<UnityEngine.UI.Text>().text = Mathf.Round(Health).ToString();
     // Debug.Log(HValue.GetComponent<UnityEngine.UI.Text>().text);
    }

    public void AddHealth(float Score, HealthAdderScript HAS) {
      Health = Health + Score;
      HAS.DestroySelf();
    }  //This is a workaround because HealthAdderScript doesn't workaround

    public void SubstractHealth(float Value) {
     Health = Health - Value;
    }
}
