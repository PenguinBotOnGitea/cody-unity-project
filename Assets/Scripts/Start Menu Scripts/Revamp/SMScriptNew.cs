using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMScriptNew : MonoBehaviour
{
    private Transform TS;
    public BulletFireNew BFN;
    // Start is called before the first frame update
    void Start()
    {
     TS = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
     if (TS.position.x == -3.0f) {
      BFN.InstantiateBullet();
     }
    }

    public void KillSelf() {
     Destroy(gameObject);
    }
}
