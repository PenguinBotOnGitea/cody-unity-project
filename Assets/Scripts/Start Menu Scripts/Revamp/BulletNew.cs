using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletNew : MonoBehaviour
{
    public string EnemyTarget;
    public SMScriptNew SMN;
    // Start is called before the first frame update
    void Start()
    {
     GetComponent<Rigidbody2D>().AddForce(new Vector2(-200,0));
     SMN = GameObject.FindWithTag(EnemyTarget).GetComponent<SMScriptNew>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D Touch) {
     if (Touch.gameObject.tag == EnemyTarget) {
      SMN.KillSelf();
     }
    }
}
