﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseEndScript : MonoBehaviour
{
    public bool IsPaused;
    public bool IsOver;
    public GameObject PB;
    public PausePanelScript PPS;
    public GameOPanelScript GOP;
    public string MenuScene;
    // Start is called before the first frame update
    void Start()
    {
     IsPaused = false;   
    }

    // Update is called once per frame
    void Update()
    {
     if (IsPaused == true)
     {
      PPS.ShowPanel();
      Time.timeScale = 0;
     }   
     else if (IsPaused == false)
     {
      PPS.HidePanel();
      Time.timeScale = 1;
     }
     if (IsOver == true)
     {
      PB.SetActive(false);
      PPS.HidePanel();
      GOP.Show();
      IsPaused = true;
     }
    }

    public void PauseToggle()
    {
     if (IsPaused == true)
     {
      IsPaused = false;
     }
     else if (IsPaused == false)
     {
      IsPaused = true;     
     }
    }

    public void Restart()
    {
     Application.LoadLevel(Application.loadedLevel);
    }

    public void CommitBackToMenu()
    {
     Application.LoadLevel(MenuScene);
    }
}
