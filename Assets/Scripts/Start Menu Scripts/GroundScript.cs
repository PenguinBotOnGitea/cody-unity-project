using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScript : MonoBehaviour
{
    public Rigidbody2D LRB;
    public string Border;
    public Vector2 Force;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D Touch)
    {
     if (Touch.gameObject.name == Border)
     {
      //Debug.Log("Go up"); Used for debugging, disabled to prevent console spam
      LRB.AddForce(Force);
     }
    }

    // Used for debugging DATE=19/06/2021
    //void OnCollisionEnter2D(Collision2D Touch)
    //{
     //Debug.Log(Touch);
    //}
}
