﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public Transform PTS;
    public Transform LTS;
    public PlayerMovementScript PMS;
    public int Damage;
    public Rigidbody2D LRB;
    public Vector2 EnemyMoveForce;
    public Vector2 EnemyUpForce;
    public bool EnemyOnGround;
    public PlayerHealthScript PHS;
    public WaveScript WS;
    public string Bmanager;
    public bool RHealthCheck;
    public ScoreScript SS;
    public string SSTag;
    public float ScoreValue;
    public string BTag;
    private bool ScoreCheck;
    private bool JumpCheck;
    public Transform TTS;
    // Start is called before the first frame update
    void Start()
    {
     PTS = GameObject.FindWithTag("Player").GetComponent<Transform>();
     PMS = GameObject.FindWithTag("Player").GetComponent<PlayerMovementScript>();
     PHS = GameObject.FindWithTag("BManager").GetComponent<PlayerHealthScript>();
     TTS = GameObject.FindWithTag("Tree").GetComponent<Transform>();
     WS = GameObject.FindWithTag(Bmanager).GetComponent<WaveScript>();
     RHealthCheck = true;
     SS = GameObject.FindWithTag(SSTag).GetComponent<ScoreScript>();
     ScoreCheck = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
     if (LTS.position.x > TTS.position.x)
     {
      LTS.rotation = Quaternion.Euler(LTS.rotation.x,180,LTS.rotation.z);
      EnemyMoveLeft();
     }
     else if (LTS.position.x < TTS.position.x)
     {
      LTS.rotation = Quaternion.Euler(LTS.rotation.x,0,LTS.rotation.z);
      EnemyMoveRight();
     }
    }

    void OnTriggerEnter2D(Collider2D Touch)
    {
      if (Touch.gameObject.tag == "Player")
     {
      CheckHealth();
     }
      if (Touch.gameObject.tag == BTag) {
       DDead(true);
      }
      if (Touch.gameObject.tag == "Border") {
       Destroy(gameObject);
       WS.ReduceEnemy();
      }
    }

    public void EnemyMoveLeft()
    {
     LTS.Translate(EnemyMoveForce * Time.deltaTime);
    }

    public void EnemyMoveRight()
    {
     LTS.Translate(EnemyMoveForce * Time.deltaTime);
    }

    void OnTriggerExit2D(Collider2D Touch)
    {
     if (Touch.gameObject.tag == "Ground")
     {
      EnemyOnGround = false;
      JumpCheck = true;
     }
    }

    public void EnemyMoveUp()
    {
     if (JumpCheck == false) {
      LRB.AddForce(EnemyUpForce);
      JumpCheck = true;
     }
    }

    void OnCollisionStay2D(Collision2D Touch)
    {
     if (Touch.gameObject.tag == "Ground")
    {
     EnemyOnGround = true;
     JumpCheck = false;
    }

    }

    public void DDead(bool Hit)
    {
      if (Hit == true) {
        SS.AddScorePlayer(ScoreValue);
        }
     Destroy(gameObject);
     if (ScoreCheck == true) {
      WS.ReduceEnemy();
      ScoreCheck = false;
     }
    }

    public void CheckHealth() {
    if (RHealthCheck == true) {
       PHS.Health = PHS.Health - Damage;
       RHealthCheck = false;
       DDead(false);
      }
    }
}
