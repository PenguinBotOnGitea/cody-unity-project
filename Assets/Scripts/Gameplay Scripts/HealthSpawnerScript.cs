using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSpawnerScript : MonoBehaviour
{
    //Variable goes here
    public float SpawnTimer;
    public Transform PTS;
    public string PTag;
    public GameObject HealthPrefab;
    // Start is called before the first frame update
    void Start()
    {
     PTS = GameObject.FindWithTag(PTag).GetComponent<Transform>();
     StartCoroutine(SpawnTime(SpawnTimer));
    }

    // Update is called once per frame
    void Update()
    {
     //StartCoroutine(SpawnTime(SpawnTimer));
     //StopCoroutine(SpawnTime(SpawnTimer));
    }

    IEnumerator SpawnTime(float tama){
     yield return new WaitForSeconds(tama);
     Instantiate(HealthPrefab, new Vector2(Random.Range(PTS.position.x-10,PTS.position.x+10),10), Quaternion.Euler(0,0,0));
     //StopCoroutine(SpawnTime(SpawnTimer));
     //Debug.Log("STOP IT");
    }
}
