//This scripts needs some tidying!
//Determining next wave's enemies is still a mess and need some more work!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveScript : MonoBehaviour
{
    public float SpawnDelay;
    public float Wave;
    public GameObject Enemies;
    public Transform PortalL;
    public Transform PortalR;
    public float WaveTime;
    public float WhichP;
    public float EnemiesThisWave;
    public GameObject Player;
    public GameObject WText;
    public GameObject DumbEnemyR;
    public GameObject DumbEnemyL;
    public float WhichEnemy;
    public float EnemiesLeft; //Used to limit the amount of enemies spawning each round
    public float MaxEnemies;
    public float LastMaxEnemies;
    public GameObject EText;
    public bool AllowedToStopTimer;
    public float SpawnedEnemy;
    // Start is called before the first frame update
    void Start()
    {
     Wave = 1;
     EnemiesThisWave = 0;
     EnemiesLeft = 0;
     AllowedToStopTimer = false;
     SpawnedEnemy = 0;
     StartCoroutine(WaveTimer(WaveTime));
     CheckEnemies();
    }

    // Update is called once per frame
    void Update()
    {
     WText.GetComponent<UnityEngine.UI.Text>().text = Wave.ToString();
     EText.GetComponent<UnityEngine.UI.Text>().text = EnemiesLeft.ToString();
     if (AllowedToStopTimer == true)
     {
      if (EnemiesLeft <= 0)
      {
       if (SpawnedEnemy >= MaxEnemies) {
        StopCoroutine(WaveTimer(WaveTime));
        AfterWave();
        //Debug.Log("Restarting wave..");
        StartCoroutine(WaveTimer(WaveTime));
       }
      }
     }
    }

    public void CheckEnemies()
    {
     if (EnemiesThisWave < MaxEnemies)
     {
      //Debug.Log(EnemiesLeft);
      ProceedSpawn();
     }
     else if (EnemiesThisWave >= MaxEnemies)
     {
      AllowedToStopTimer = true;
      Debug.Log("No enemies will be spawning this round");
     }
    }

    public float WhichPortal()
    {
     WhichP = Random.Range(1,3);
     //Debug.Log(WhichP);
     return WhichP;
    }

    public void Wrapper()
    {
     float LoR = WhichPortal();
     if (LoR >= 2) {
      SpawnL(Enemies);
     }
     else if (LoR <= 2) {
      SpawnR(Enemies);
     }
    }

    public void ProceedSpawn()
    {
     if (WhichP <= 2)
     {
      EnemiesThisWave = EnemiesThisWave + 1; //Decreasing the variable so there's a limit to enemies each round
      EnemiesLeft = EnemiesLeft + 1;
      SpawnedEnemy = SpawnedEnemy + 1;
      Wrapper();
     }
     else if (WhichP >= 2)
     {
      EnemiesThisWave = EnemiesThisWave + 1; //View previous comment
      EnemiesLeft = EnemiesLeft + 1;
      SpawnedEnemy = SpawnedEnemy + 1;
      Wrapper();
     }
    }

    public void SpawnL(GameObject EnemyToBeSpawned) //Spawning from left portal, right portal coming soon
    {
     Instantiate(EnemyToBeSpawned, new Vector2(PortalL.position.x,PortalL.position.y), Quaternion.Euler(0,0,0));
     StartCoroutine(DelaySpawn()); //Delaying next spawn
    }

    public void SpawnR(GameObject EnemyToBeSpawned) //Spawning from right portal, right portal coming soon
    {
     Instantiate(EnemyToBeSpawned, new Vector2(PortalR.position.x,PortalR.position.y), Quaternion.Euler(0,0,0));
     StartCoroutine(DelaySpawn()); //Delaying next spawn
    }

    IEnumerator DelaySpawn()
    {
     yield return new WaitForSeconds(SpawnDelay); //Delay between enemies spawning
     CheckEnemies(); //Restarting spawn process
    }

    IEnumerator WaveTimer(float Time) //The wave timer
    {
     LastMaxEnemies = MaxEnemies;
     yield return new WaitForSeconds(Time); //The wave's timer
     AfterWave();
    }

    public void AfterWave()
    {
     MaxEnemies = LastMaxEnemies + 1; //Changing from * 2 to + 1 since 2 times the enemy of the last wave isn't always a good idea
     EnemiesThisWave = 0;
     CheckEnemies();
     Wave = Wave + 1;
     AllowedToStopTimer = false;
     StartCoroutine(WaveTimer(WaveTime));
    }

    public void ReduceEnemy() {
      EnemiesLeft = EnemiesLeft - 1;
      //Debug.Log("EnemiesLeft:");
      //Debug.Log(EnemiesLeft);
    }
}
