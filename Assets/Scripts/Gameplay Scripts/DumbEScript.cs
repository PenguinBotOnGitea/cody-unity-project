using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DumbEScript : MonoBehaviour
{
    // Variables go here
    public PlayerHealthScript PHS;
    public string PTag;
    public float Damage;
    public Rigidbody2D LRB;
    public Vector2 Force;
    public bool EnemyOnGround;
    public Transform PTS;
    public Transform LTS;
    public Vector3 Orientation;
    public string GTag;
    public Vector2 JumpForce;
    public GameObject Self;
    public string BMTag;
    public string BTag;
    public WaveScript WS;
    public string Bmanager;
    public bool RHealthCheck;
    public ScoreScript SS;
    public string SSTag;
    public float ScoreValue;
    private bool ScoreCheck;
    private bool JumpCheck;
    // Start is called before the first frame update
    void Start()
    {
     EnemyOnGround = false;
     PTS = GameObject.FindWithTag(PTag).GetComponent<Transform>();
     PHS = GameObject.FindWithTag(BMTag).GetComponent<PlayerHealthScript>();
     LTS.rotation = Quaternion.Euler(Orientation);
     WS = GameObject.FindWithTag(Bmanager).GetComponent<WaveScript>();
     RHealthCheck = true;
     SS = GameObject.FindWithTag(SSTag).GetComponent<ScoreScript>();
     ScoreCheck = true;
    }

    // Update is called once per frame
    void Update()
    {
     LRB.AddForce(Force * Time.deltaTime);
     if (EnemyOnGround == true && JumpCheck == false)
     {
      LRB.AddForce(JumpForce);
      JumpCheck = true;
     }
     else if (EnemyOnGround == false)
     {
      //Debug.Log("Do nothing");
     }
    }

    void OnTriggerEnter2D(Collider2D Touch)
    {
     if (Touch.gameObject.tag == PTag)
     {
      HealthCheck();
     }
     if (Touch.gameObject.tag == BTag)
     {
      DDead(true);
     }
     if (Touch.gameObject.tag == "Border") {
      Destroy(gameObject);
      WS.ReduceEnemy();
     }
    }

    void OnTriggerStay2D(Collider2D Touch)
    {
     if (Touch.gameObject.tag == GTag)
     {
      EnemyOnGround = true;
      JumpCheck = false;
     }
    }

    void OnTriggerExit2D(Collider2D Touch)
    {
     if (Touch.gameObject.tag == GTag){
      EnemyOnGround = false;
      JumpCheck = true;
     }
    }

    public void DDead(bool Hit)
    {
      if (Hit == true) {
        SS.AddScorePlayer(ScoreValue);
        }
     Destroy(gameObject);
     if (ScoreCheck == true) {
      WS.ReduceEnemy();
      ScoreCheck = false;
     }
    }

    public void HealthCheck() {
     if (RHealthCheck == true) {
      PHS.Health = PHS.Health - Damage;
      RHealthCheck = false;
      DDead(false);
     }
    }
}
