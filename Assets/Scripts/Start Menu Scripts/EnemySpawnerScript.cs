using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerScript : MonoBehaviour
{
    public GameObject Enemies;
    // Start is called before the first frame update
    void Start()
    {
     StartCoroutine(EnemyWait(3));
    }

    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator EnemyWait(float Time) {
     //Debug.Log("SPAWN DAMN IT");
     Instantiate(Enemies, new Vector2(transform.position.x, transform.position.y), Quaternion.Euler(0,0,0));
     yield return new WaitForSeconds(Time);
     StartCoroutine(EnemyWait(3));
    }

    void OnTriggerEnter2D(Collider2D Touch) {
     if (Touch.gameObject.tag == "Bullet") {
      Debug.Log("adafasfsa");
      Destroy(Touch.gameObject);
     }
    }
}
