using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireButtonScript : MonoBehaviour
{
    //Variables are here
    public Transform PT;
    public GameObject Bullets;
    public float XAdder;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Fire()
    {
     //Debug.Log("Proceed to run the Fire function..");
     CheckDirection();
     Instantiate(Bullets, new Vector2(PT.position.x + XAdder, PT.position.y), Quaternion.Euler(0, 0, PT.rotation.z));
    }


    public void CheckDirection()
    {
     if (PT.rotation.z >= 0)
     {
      XAdder = 1.5f;
     }
     else if (PT.rotation.z >= -180)
     {
      XAdder = -1.5f;
     }
    }
}
