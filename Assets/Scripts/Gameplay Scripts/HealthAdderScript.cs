using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthAdderScript : MonoBehaviour
{
    // This is where global variables are stored
    public PlayerHealthScript PHS;
    private bool TagCheck;
    public float TimeBeforeGone;
    public string BTag;
    public string PTag;
    public float Score;
    private HealthAdderScript HAS;
    public string ETag;
    // Start is called before the first frame update
    void Start()
    {
     TagCheck = false;
     PHS = GameObject.FindWithTag(BTag).GetComponent<PlayerHealthScript>();
     StartCoroutine(Timer(TimeBeforeGone));
     //Debug.Log(PHS);
     PHS = GameObject.FindWithTag(BTag).GetComponent<PlayerHealthScript>();
     HAS = gameObject.GetComponent<HealthAdderScript>();
    }

    IEnumerator Timer(float Time){
     yield return new WaitForSeconds(Time);
     Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D Touch){
     if (Touch.gameObject.tag == PTag && TagCheck == false){
      PHS.AddHealth(Score, HAS);
      //Debug.Log("Test!");
      TagCheck = true;
     }
     else if (Touch.gameObject.tag == ETag && TagCheck == false) {
      gameObject.SetActive(false);
    };
     //Debug.Log("Touched!");
    }

    public void DestroySelf(){
     Destroy(gameObject);
    }
}
