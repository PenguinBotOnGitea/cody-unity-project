using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderScript : MonoBehaviour
{
    public PlayerMovementScript PMS;
    public string PTag;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D Touch) {
     Debug.Log(Touch);
     if (Touch.gameObject.tag == PTag) {
      PMS.ReturnToBase();
     }
    }
}
