using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMEnemyScript : MonoBehaviour
{
    public string Gron;
    public Vector2 Force;
    public string Trigger;
    public EnemyDetectorScript EDS;
    // Start is called before the first frame update
    void Start()
    {
     EDS = GameObject.Find("Square").GetComponent<EnemyDetectorScript>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D Touch) {
     if (Touch.gameObject.name == Gron) {
      MoveRight();
     }
    }

    private void MoveRight() {
     GetComponent<Rigidbody2D>().AddForce(Force);
    }

    void OnTriggerEnter2D(Collider2D Touch) {
     if (Touch.gameObject.name == Trigger) {
      //Debug.Log("PLEASE CHANGE VALUE");
      EDS.CheckBool(true);
      Debug.Log(EDS.Check);
     }
   }
}
